#include "ChannelUSB.h"
#include <QDateTime>
#include <QDir>
#include <QFileInfo>
#include "Json.h"
#include <QVariant>

ChannelUSB::ChannelUSB(QObject *parent) : Channel(parent)
{
    alsa=Link::create("InputAlsa");
    audio=Link::create("Resample");
    video=Link::create("DecodeV");
    encA=Link::create("EncodeA");
    encV=Link::create("EncodeV");
    encV2=Link::create("EncodeV");
    usb=Link::create("InputV4l2");
    camOpen=false;
    connect(usb,SIGNAL(newEvent(QString,QVariant)),this,SLOT(onNewEvent(QString,QVariant)));
}

void ChannelUSB::init(QVariantMap)
{

    usb->linkV(video);
    overlay->linkV(encV);
    overlay->linkV(encV2);


    alsa->linkA(audio);
    gain->linkA(encA);


    Channel::init();
    presetPath=QString("/link/config/misc/preset_ch%1.json").arg(id);
    if(QFile::exists(presetPath))
        presetList=Json::loadFile(presetPath).toList();
    else
    {
        QVariantList list;
        list<<0<<0<<100;
        for(int i=0;i<8;i++)
        {
            presetList<<list;
        }
    }

}

void ChannelUSB::updateConfig(QVariantMap cfg)
{

    if(cfg["enable"].toBool())
    {
        audio->start();
        video->start();
        if(cfg.contains("alsa"))
        {
            QVariantMap dataAlsa;
            dataAlsa["path"] = cfg["alsa"].toString();
            if(cfg.contains("lnk"))
                dataAlsa["lnk"] = cfg["lnk"].toString();
            if(cfg.contains("channels"))
                dataAlsa["channels"] = cfg["channels"].toInt();
            alsa->start(dataAlsa);
        }
        else
            alsa->start();

        {
            QVariantMap dd;
            if(cfg.contains("capture"))
            {
                dd["width"]=cfg["capture"].toMap()["width"].toInt();
                dd["height"]=cfg["capture"].toMap()["height"].toInt();
                dd["framerate"]=cfg["capture"].toMap()["framerate"].toInt();
            }
            else
            {
                dd["width"]=cfg["encv"].toMap()["width"].toInt();
                dd["height"]=cfg["encv"].toMap()["height"].toInt();
                dd["framerate"]=cfg["encv"].toMap()["framerate"].toInt();
            }
            if(cfg.contains("uvc"))
            {
                dd["path"]="";
                dd["lnk"]=cfg["uvc"];
            }
            usb->start(dd);
        }

        if(cfg["encv"].toMap()["codec"].toString()!="close")
            encV->start(cfg["encv"].toMap());
        else
            encV->stop();

        if(cfg["encv2"].toMap()["codec"].toString()!="close")
            encV2->start(cfg["encv2"].toMap());
        else
            encV2->stop();

        if(cfg["enca"].toMap()["codec"].toString()!="close")
            encA->start(cfg["enca"].toMap());
        else
            encA->stop();
    }
    else
    {
        audio->stop();
        alsa->stop();
        usb->stop();
        encV->stop();
        encV2->stop();
    }

    Channel::updateConfig(cfg);
}

bool ChannelUSB::presetSet(int index, int p, int t, int z)
{
    QVariantList args;
    args<<p<<t<<z;
    presetList[index]=args;
    Json::saveFile(presetList,"/link/config/misc/preset.json");
    return true;
}

bool ChannelUSB::presetCall(int index)
{
    QVariantList ptzList = presetList[index].toList();
    //qDebug() << ptzList;

    usb->invoke("ptz_abs_p",ptzList[0]);
    usb->invoke("ptz_abs_t",ptzList[1]);
    usb->invoke("ptz_abs_z",ptzList[2]);
    return true;
}

QVariantMap ChannelUSB::uvcInvoke(QVariantMap map)
{
    QVariant ret;
    if(!map.contains("func"))
        return QVariantMap();
    if(!map.contains("arg"))
        ret = usb->invoke(map["func"].toString());
    else
        ret =usb->invoke(map["func"].toString(),map["arg"]);

    if(ret.type() != QVariant::Map)
        return QVariantMap{{"data",ret}};
    return ret.toMap();
}

void ChannelUSB::onNewEvent(QString type, QVariant info)
{
    if(sender()==usb && type=="CameraInfo")
        camInfo = info.toMap();

    else if(sender()==usb && type=="CameraOpen")
    {
        camOpen=info.toBool();
        emit newEvent(type,info);
    }
}
