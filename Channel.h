#ifndef CHANNEL_H
#define CHANNEL_H

#include <QObject>
#include "Link.h"
#include <QMap>
#include <QTimer>
#include "Json.h"

struct Stream
{
    QString streamPath;
    QString streamSuffix;
    LinkObject *mux;
    LinkObject *udpServer;
    static LinkObject *httpServer;
    static LinkObject *rtspServer;
    static LinkObject *webrtcServer;

    Stream(QString type, QString suffix, QString chnId) : streamSuffix(suffix)
    {
        mux = nullptr;
        udpServer = nullptr;
        QString streamFormat;

        if (type != "ndi")
        {
            mux = Link::create("Mux");
            if (type == "ts")
            {
                udpServer = Link::create("TSUdp");
                mux->linkV(udpServer);
            }
        }
        else
        {
        #if !defined HI3516E && !defined HI3516CV610
            mux = Link::create("NDISend");
        #endif
        }

        if(httpServer==NULL)
        {
            httpServer=Link::create("TSHttp");
            QVariantMap httpData;
            httpData["TSBuffer"]=0;
            httpServer->start(httpData);
        }

        if(rtspServer==NULL)
        {
            rtspServer=Link::create("Rtsp");
            rtspServer->start();
        }

        if(webrtcServer==NULL)
        {
        #if !defined(HI3521D) && !defined (HI3531D)
            webrtcServer=Link::create("WebRTC");
        #endif
        }


        if (mux != nullptr)
        {
            if (type == "rtmp")
            {
                streamPath = "rtmp://127.0.0.1/live/" + streamSuffix;
                streamFormat = "flv";
            }
            else if (type == "push")
            {
                streamPath = "rtmp://127.0.0.1/live/test" + chnId;
                streamFormat = "flv";
            }
            else if (type == "hls")
            {
                streamPath = "/tmp/hls/" + streamSuffix + ".m3u8";
                streamFormat = "hls";
            }
            else if (type == "srt")
            {
                streamPath = "srt://:" + QString::number(9000) + "?mode=listener&latency=50";
                streamFormat = "mpegts";
            }
            else if (type == "ts" || type == "rtsp" || type == "webrtc")
            {
                streamPath = "mem://" + streamSuffix;
                streamFormat = (type == "ts") ? "mpegts" : type;
            }

            QVariantMap data;
            data["path"] = streamPath;
            data["format"] = streamFormat;
            mux->setData(data);
        }
    }
};


class Channel : public QObject
{
    Q_OBJECT
public:
    explicit Channel(QObject *parent = 0);
    virtual void init(QVariantMap cfg=QVariantMap());
    virtual void updateConfig(QVariantMap cfg);
    QString writeCom(const QString &com);
    QString doSnap(const int &mod = 0,const QVariantList &chnIds = QVariantList());
    QString type;
    bool enableAVS;    
    bool isSrcLine;
    int lastAId;
    int id;
    QString name;

    static LinkObject *lineIn;
    static LinkObject *lineOut;
    static LinkObject *alsa;
    static LinkObject *ndiRecv;
    QVariantMap data;
    LinkObject *audio;
    LinkObject *video;
    LinkObject *volume;
    LinkObject *encA;
    LinkObject *encV;
    LinkObject *gain;

    LinkObject *overlay;
    LinkObject *snap;
    bool enable;
    LinkObject *encV2;
    QMap<QString,Stream*> streamMap;
    QMap<QString,Stream*> streamMap_sub;
};

#endif // CHANNEL_H
