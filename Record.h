#ifndef RECORD_H
#define RECORD_H

#include <QObject>
#include <QVariantMap>
#include <QDebug>
#include <QTimer>
#include "Config.h"

struct RecFormat
{
    bool hadRec;
    int startNum;
    int fileCount;
    LinkObject *mux;
};

struct RecChn
{
    int chnId;
    QString chnName;
    int startTime;
    QString durTime;
    bool hadPause;
    int pauseCount;
    QString curFileName;
    QTimer *pauseTimer;
    QTimer *snapTimer;
    QString snapPath;
    int snapCount;
    QMap<QString,RecFormat*> recFormats;
};

class Record : public QObject
{
    Q_OBJECT
public:
    explicit Record(QObject *parent = 0);

    QVariantMap config;
    void init();
    void resetEnc();
private:
    QString rootPath = "/root/usb";
    QList<RecChn*> recChns;
    QTimer *clearTimer = nullptr;

    void restConf();
    int getLoopRecIndex(const QString &type);
    QString writeCom(const QString &com);

signals:
    void newEvent(QString type, QVariant msg);

public slots:
    bool update(QString json);
    bool execute(const QString &json);
    QString getDurTime();
    QVariantMap getState();
    bool start();
    bool stop();
    bool isRecordState();
    bool isMountDisk();
    QVariantList getRecChnsState();
    void onClearRecFile();
};
extern Record *GRecord;
#endif // RECORD_H
