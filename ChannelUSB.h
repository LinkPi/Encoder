#ifndef CHANNELUSB_H
#define CHANNELUSB_H

#include "Channel.h"

class ChannelUSB : public Channel
{
    Q_OBJECT
public:
    explicit ChannelUSB(QObject *parent = 0);
    virtual void init(QVariantMap);
    virtual void updateConfig(QVariantMap cfg);
    LinkObject *usb;
    LinkObject *alsa;
    QString presetPath;
    QVariantList presetList;
    bool camOpen;
    QVariantMap camInfo;
signals:
    void newEvent(QString,QVariant);
public slots:
    bool presetSet(int index,int p,int t,int z);
    bool presetCall(int index);
    QVariantMap uvcInvoke(QVariantMap map);
    void onNewEvent(QString type,QVariant info);
};

#endif // CHANNELUSB_H
